package clock

import (
	"testing"
	"time"
)

func TestMock(t *testing.T) {
	dateTime := time.Date(1999, time.December, 30, 9, 27, 12, 0, time.UTC)

	mock := Mock{}
	mock.SetDateTime(dateTime)

	if mock.Now() != dateTime {
		t.Fatalf("wanted %v; got: %v", dateTime, mock.Now())
	}
}

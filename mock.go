package clock

import "time"

type Mock struct {
	dateTime time.Time
}

func (m *Mock) Now() time.Time {
	return m.dateTime
}

func (m *Mock) SetDateTime(dateTime time.Time) {
	m.dateTime = dateTime
}
